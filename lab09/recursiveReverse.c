
#include "lab09.h"

void swapString(void **a, void **b){
	void* temp;

	temp = *a;

	*a = *b;
	*b = temp;	
}

void recursiveReverse(void **listPtr, int lowIndex, int highIndex, void (*swapFunction) (void**, void**)){
	int i;

	if(highIndex > lowIndex){
		swapFunction(&(listPtr[lowIndex]), &(listPtr[highIndex]));
		recursiveReverse(listPtr, lowIndex+1, highIndex-1, swapFunction);
	}
}
