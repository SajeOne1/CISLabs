

/* you are not submitting the lab09.h file, so do not change this file */


/*****
* Standard Libraries
*****/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>


/*****
* Function Prototypes for recursiveReverse.c
*****/

/* Swap the value of two strings */
void swapString(void **a, void **b);

/* This is the recursiveReverse function that will reverse the order of a list */
void recursiveReverse(void **listPtr, int lowIndex, int highIndex, void (*swapFunction) (void **, void **));

