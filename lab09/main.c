

#include "lab09.h"


int main(int argc, char **argv) {
	void** listptr;
	int i;
	
	listptr = (void*)argv;
   	
	if(argc > 1){
		for(i = 1; i < argc; i++){
			printf("before: %s\n", argv[i]);
		}
		recursiveReverse(listptr, 1, argc-1, swapString);	
		for(i = 1; i < argc; i++){
			printf("after: %s\n", argv[i]);
		}
	}else{
		printf("usage: word1 ...\n");
	}
    
    return 0;
}
