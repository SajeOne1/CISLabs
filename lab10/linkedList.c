

#include "lab10.h"


/* Add a node to the front (push function). */
void addToFront(Node **head, Node *toAdd) {
    /* You can assume that the node toAdd has already been allocated and is not empty */
	toAdd->next = (*head);
	(*head) = toAdd;
}

/* Remove a node from the front (pop function). Return the node that is removed from the list. */
Node *removeFromFront(Node **head) {
    /* Hint: remember to check if the linked list is empty. */
	Node* to_delete;	

	to_delete = *head;
	*head = (*head)->next;

    return to_delete;
}

