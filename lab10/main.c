
/* Lab header file */
#include "lab10.h"

/* Testing prototypes */
#include "testing_10.h"

/* Here is the main function for testing */
int main(int argc, const char *argv[]) {
    char *result[] = {"FAIL", "PASS"};
    int score = 0;
    
    printf("Lab 10: CIS*2500 Review \n");
    printf("\t - Many hardcoded unit tests are included for your use in this lab, no need for you to change main.\n"); 
    printf("\t - Any failed test cases will be printed for you \n\n");
    
    /*** Uncomment the tests when you are ready to begin testing ***/
    score += testOne(result);
    score += testTwo(result);
    score += testThree(result);
    score += testFour(result, argc, argv);
    score += testFive(result);
    score += testSix(result);
    
    printf("Your score = %d/18\n", score);
    
    return 0;
}

