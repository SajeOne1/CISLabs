
/* This ifndef will stop the pesky "previous definition"/"redefinition" error when the preprocessor reads the file multiple times */
#ifndef LAB_10

/* Define this library */
#define LAB_10

/*****
* Standard Libraries
*****/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>

/*****
* Struct Definitions
*****/

/* enum and typedef of a boolean type */
typedef int boolean;
enum boolean{false, true};

/* Linked list node and typedef */
typedef struct listNode Node;
struct listNode {
    char *data;
    Node *next;
};


/*****
* Function Prototypes
*****/


/*** 1. Passing parameters ***/

/* Return the value of num squared */
int squared_value(int num);

/* Set the value of &num to be num squared */
void squared_reference(int *num);


/*** 2. Arrays ***/

/* Calculate the sum of a single column in an array with 8 rows and 7 columns */
int sumColumn(int array[8][7], int column);

/*** 3. Dynamic Memory ***/

/* Allocate enough memory, and copy the */
char *copyString(const char *toCopy);


/*** 4. Command Line Arguments ***/

/* Allocate the correct amount of memory for storing the string value of all strings passed */
void storeCommandLineArgs(int argc, const char *argv[], char ***args);


/*** 5. Structs ***/

/* Allocate memory for a single Node struct and return a pointer to said struct */
Node *newNode(char *string);

/* Free any associated memory with a Node, including the node itself */
void deleteNode(Node **toDelete);


/*** 6. Linked Lists ***/

/* Add a node to the front (push function). You can assume the node is already allocated */
void addToFront(Node **head, Node *toAdd);

/* Remove a node from the front (pop function). Return the node that is removed from the list. */
Node *removeFromFront(Node **head);

#endif
