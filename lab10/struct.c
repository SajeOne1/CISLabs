

#include "lab10.h"


/* Allocate memory for a single Node struct and return a pointer to said struct */
Node *newNode(char *string) {
    /* Hint: You should allocate memory for the new node, and the pointer in the node */
	Node* new_node;

	new_node = malloc(sizeof(Node));

	new_node->data = malloc(strlen(string));
	strcpy(new_node->data, string);

	new_node->next = NULL;
    
    return new_node;
}

/* Free any associated memory with a Node, including the node itself */
void deleteNode(Node **toDelete) {
    /* Hint: Make sure you set the node you free to NULL!*/

	free((*toDelete)->data);
	free(*toDelete);

	(*toDelete) = NULL;
}
