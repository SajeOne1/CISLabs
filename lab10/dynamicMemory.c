

#include "lab10.h"


/* Allocate enough memory, and copy the */
char *copyString(const char *toCopy) {
	char* heap_str;    

	heap_str = malloc(strlen(toCopy));
	strcpy(heap_str, toCopy);

	return heap_str;
}
