
#include "lab10.h"

#ifndef TESTING_10

#define TESTING_10

/* Return the highest integer value */
int max(int one, int two);

/* Number 1: Passing Parameters */
int testOne(char *result[]);

/* Number 2: Arrays */
int testTwo(char *result[]);

/* Number 3. Dynamic memory allocation */
int testThree(char *result[]);

/* Number 4. Command line Arguments */
int testFour(char *result[], int argc, const char *argv[]);

/* Number 5. Structs */
int testFive(char *result[]);

/* Number 6. Linked list tests */
int testSix(char *result[]);


#endif
