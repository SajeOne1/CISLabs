

#include "lab10.h"

/* Calculate the sum of a single column in an array. 8 rows and 7 columns */
int sumColumn(int array[8][7], int column) {
	int i, sum = 0;

	for(i = 0; i < 8; i++){
		sum += array[i][column];		
	}
    return sum;
}
