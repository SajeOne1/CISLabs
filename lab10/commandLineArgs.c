

#include "lab10.h"


void storeCommandLineArgs(int argc, const char *argv[], char ***args) {
    /* Hint: you have already written a copy string function */
	int i;

	for(i = 0; i < argc; i++){
		(*args) = realloc((*args), sizeof(char*) * (i+1));
		(*args)[i] = copyString(argv[i]);
	}
}
