

#include "lab10.h"


/* Return the value of num squared */
int squared_value(int num) {
	return num*num;
}

/* Set the value of &num to be num squared */
void squared_reference(int *num) {
	(*num) = (*num) * (*num);
}

