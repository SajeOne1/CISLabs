
#include "lab10.h"
#include "testing_10.h"


int max(int one, int two) {
    if (one > two) {
        return one;
    } else {
        return two;
    }
}

/* Number 1: Passing Parameters */
int testOne(char *result[]) {
    int passByValue = 0;
    int passByReference = 4;
    int score = 0;
    
    printf("*** 1. Testing pass by value vs. pass by reference\n");
    passByValue = squared_value(4);
    squared_reference(&passByReference);
    
    score += (passByValue == 16);
    score += (passByReference == 16);
    
    printf("\t pass by value result = %s \n", result[passByValue == 16]);
    printf("\t pass by reference result = %s \n", result[16 == passByReference]);
    printf("\t\t score = %d of 2\n\n", score);
    
    return score;
}


/* Number 2: Arrays */
int testTwo(char *result[]) {
    int array[8][7];
    int x, y;
    int score = 2;
    int sums[] = {36, 72, 108, 144, 180, 216, 252};
    boolean correctSum = true;
    
    /* Initialize array */
    for (y = 0; y < 8; y++) {
        for (x = 0; x < 7; x++) {
            array[y][x] = (x+1)*(y+1);
        }
    }
    
    printf("*** 2. Arrays\n");
    for (x = 0; x < 7; x++) {
        score -= (sumColumn(array, x) != sums[x]);
        
        if (sumColumn(array, x) != sums[x]) {
            correctSum = false;
            printf("\t FAILED: %d != %d \n", sumColumn(array, x), sums[x]);
        }
    }
    
    score = max(score, 0);
    printf("\t\t sum column result: %s\n", result[correctSum]);
    printf("\t\t score = %d of 2\n\n", score);
    
    return score;
}


/* Number 3. Dynamic memory allocation */
int testThree(char *result[]) {
    const char stringTest[] = "asdfg 98765 abc123";
    char *copyTest = NULL;
    int score;
    
    printf("*** 3. Dynamic memory allocation \n");
    copyTest = copyString(stringTest);
    
    printf("\t\t CopyString result = %s \n", result[strcmp(stringTest, copyTest) == 0]);
    score = 2*(strcmp(stringTest, copyTest) == 0);
    printf("\t\t score = %d of 2\n\n", score);
    
    free(copyTest);
    
    return score;
}


/* Number 4. Command line Arguments */
int testFour(char *result[], int argc, const char *argv[]) {
    int y, score = 4;
    char **commandArgs = NULL;
    boolean argsCopied = true;
    
    printf("*** 4. CommandLineArguments\n");
    storeCommandLineArgs(argc, argv, &commandArgs);
    for (y = 0; y < argc; y++) {
        if (strcmp(argv[y], commandArgs[y]) != 0) {
            argsCopied = false;
            printf("\t FAILED: %s != %s \n", argv[y], commandArgs[y]);
            score -= 1;
        }
    }
    
    /* Get score and free memory */
    score = max(score, 0);
    for (y = 0; y < argc; y++) {
        free(commandArgs[y]);
    }
    free(commandArgs);
    
    printf("\t\t command line arguments copied = %s\n", result[argsCopied]);
    printf("\t\t score = %d of 4\n\n", score);
    
    return score;
}

/* Number 5. Structs */
int testFive(char *result[]) {
    boolean newNodeTest = true;
    boolean deleteNodeTest = true;
    Node *list[5];
    char *strings[] = {" First \n String", "second", "stringthree", "String 4", "LaSt"};
    
    int i;
    int score, delScore = 2, newScore = 2;
    
    printf("*** 5. Structs \n");
    for (i = 0; i < 5; i++) {
        /*list[i] = newNode(strings[i]);*/
        list[i] = newNode(strings[i]);
    
        if (strcmp(strings[i], list[i]->data) != 0 || list[i]->next != NULL) {
            printf("\t FAILED: %s != %s \n", strings[i], list[i]->data);
            
            if (list[i]->next != NULL) {
                printf("\t - Don't forget about NULL...\n");
            }
            
            newNodeTest = false;
            newScore -= 1;
        }
    }
    
    for (i = 0; i < 5; i++) {
        deleteNode(&list[i]);
        
        if (list[i] != NULL) {
            printf("\t FAILED delete: for node %d: %s \n", i, strings[i]);
            printf("\t - Make sure you set the pointers to NULL after it has be freed.\n");
            deleteNodeTest = false;
            delScore -= 1;
        }
    }
    
    score = max(newScore, 0) + max(delScore, 0);
    printf("\t\t struct allocate result = %s\n", result[newNodeTest]);
    printf("\t\t linked list remove result = %s\n", result[deleteNodeTest]);
    printf("\t\t score = %d of 4\n\n", score);
    
    return score;
}

/* Number 6. Linked list tests */
int testSix(char *result[]) {
    Node *head = NULL;
    boolean addToFrontTest = true;
    boolean removeFromFrontTest = true;
    int i, score, pushScore = 2, popScore = 2;
    char *strings[] = {"one", "two", "three", "head"};
    
    printf("*** 6. Linked list\n");
    
    for (i = 0; i < 4; i++) {
        Node *new = newNode(strings[i]);
        addToFront(&head, new);
        
        if (strcmp(strings[i], head->data) != 0) {
            printf("\t FAILED add: %s != %s \n", strings[i], head->data);
            addToFrontTest = false;
            pushScore -= 1;
        }
    }
    
    i = 3;
    while (head != NULL) {
        Node *removed = removeFromFront(&head);
        
        if (strcmp(strings[i], removed->data) != 0) {
            printf("\t FAILED remove: %s != %s \n", strings[i], removed->data);
            removeFromFrontTest = false;
            popScore -= 1;
        }
        
        /* ***NEED TO FREE MEMORY HERE*** */
        deleteNode(&removed);
        
        i--;
    }
    
    score = max(pushScore, 0) + max(popScore, 0);
    printf("\t\t linked list add result = %s\n", result[addToFrontTest]);
    printf("\t\t linked list remove result = %s\n", result[removeFromFrontTest]);
    printf("\t\t score = %d of 4\n\n", score);
    
    return score;
}

