#include "schedule.h"

int main(int argc, char *argv[]) {
    char *fileName = "docs/schedule.bin";
	int length = 0;
	Event* list;
    
    createBinaryFile(fileName);

	length = readBinaryFile(fileName, &list);

	printSchedule(list, length);

	free(list);
    
    return 0;
}
