#include "schedule.h"

/* Writes to a binary file */
void createBinaryFile(char *name) {
    int i;
    FILE *ptr = fopen(name,"wb");
    char names[][64] = {"breakfast", "commute", "meeting", "lunch", "seminar", "gym", "commute", "dinner"};
    int starts[] = {8, 8, 10, 12, 13, 16, 18, 19};
    boolean halfHours[] = {false, true, false, false, false, true, false, false};
    int durations[] = {30, 45, 75, 60, 180, 80, 45, 30};
    
    
    for(i = 0; i < 8; i++) {
        Event new;
        
        sprintf(new.name, "%s", names[i]);
        new.startTime = starts[i];
        new.halfHour = halfHours[i];
        new.duration = durations[i];
        
        fwrite(&new, sizeof(Event), 1, ptr);
    }
    
    fclose(ptr);
}

/* compare function for qsort */
int compar(const void* a, const void* b){
	const Event* event_a;
	const Event* event_b;
	int diff = 0;

	event_a = a;
	event_b = b;

	diff = event_a->startTime - event_b->startTime;	

	if(!diff){
		if(event_a->halfHour == true)
			return 1;
		else if(event_b->halfHour == true)
			return -1;
	}

	return diff;
}

/* Prints a schedule of events from a list of events */
void printSchedule(Event *list, int listLength) {
	int list_iter = 0;
	boolean cur_half = false;

	int time_next = 0;
	boolean half_next = false;	

	char day_half[3];
	int time = 8;
	int minutes = 0;
	int display_time = time;

	day_half[0] = 'a';
	day_half[1] = 'm';
	day_half[2] = '\0';
	
	qsort(list, listLength, sizeof(Event), compar);

	while(time <= 21 || cur_half == true){
		display_time = time;

		if(cur_half)
			minutes = 30;
		else
			minutes = 0;
		
		if(time > 12){
			day_half[0] = 'p';
			display_time = time - 12;
		}

		if(time_next || (list[list_iter].startTime == time && cur_half == list[list_iter].halfHour)){

			printf("%d:%02d %s - %s\n", display_time, minutes, day_half, list[list_iter].name);

			if(!time_next){
				if((list[list_iter].duration/60) > 0){
					time_next = time + list[list_iter].duration / 60;
					if(list[list_iter].duration % 60 == 0){
						time_next--;
					}	
					if((time_next - time) % 2 == 0){
						if(list[list_iter].halfHour == true)
							half_next = false;
						else
							half_next = true;
					}else{
						half_next = list[list_iter].halfHour;
					}
				}else if(list[list_iter].duration > 30){
					if(cur_half == false)
						time_next = time;	
					else
						time_next = time+1;

					if(list[list_iter].halfHour == true)
						half_next = false;
					else
						half_next = true;

				}
			}else{
				if(time == time_next && cur_half == half_next){
					time_next = 0;
				}
			}

			if(!time_next)
				list_iter++;

		}else{
			printf("%d:%02d %s - \n", display_time, minutes, day_half);
		}
		
		if(cur_half){
			cur_half = false;
			time++;
		}else{
			cur_half = true;
		}
		
	}
}

/* Reads the content from a binary file named fileName. Returns the length of the list. */
int readBinaryFile(char *fileName, Event **list) {
    int listLength = 0;
	long byteLength = 0;
	FILE* fp;

	fp = fopen(fileName, "rb");
	fseek(fp, 0, SEEK_END);
	byteLength = ftell(fp);
	rewind(fp);

	listLength = byteLength / sizeof(Event);

	*list = calloc(listLength, sizeof(Event));

	fread(*list, sizeof(Event), listLength, fp);

	fclose(fp);

    return listLength;
}

