
/* You can add anything you would like to the header file but DO NOT DELETE ANYTHING */

/*****
* Standard Libraries
*****/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>


/*****
* Self-defined Data Structures
*****/

/* enumerate a Boolean datatype for convenience */
typedef int abstractBoolean;
enum abstractBoolean {
	false,
	true
};
typedef abstractBoolean boolean;

/* Start time is in hours, duration is in minutes */
typedef struct eventNode {
    char name[64];
    int startTime;
    boolean halfHour;
    int duration;
} Event;


/*****
* Function Prototypes for schedule.c
*****/

/***** The only stipulation of this lab is that you must make these functional *****/

/* Writes to a binary file */
void createBinaryFile(char *fileame);

/* Prints a schedule of events from a list of events */
void printSchedule(Event *list, int listLength);

/* Reads the content from a binary file named fileName into the Event list pointer. Returns the number of Events. */
int readBinaryFile(char *fileName, Event **list);


